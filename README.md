# Android一个简单的闹钟

#### 介绍
能够动态添加闹钟，并注册服务，保证应用可以再后台进行提醒
缺点：如果系统因内存不足，被系统回收则无法正常运行
注：系统功能暂未完全实现，持续迭代


#### 效果图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1119/192613_bbae6da0_8759392.png "屏幕截图 2021-11-18 220206.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1119/192630_b94d78a4_8759392.png "屏幕截图 2021-11-18 220238.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1119/192639_68eda1ee_8759392.png "屏幕截图 2021-11-18 220221.png")

 :two_hearts:   :two_hearts:   :two_hearts: 
