package com.franzliszt.alarmclock.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.franzliszt.alarmclock.R;
import com.franzliszt.alarmclock.adapter.TestAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.we.swipe.helper.WeSwipe;

public class TestActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private List<String> strings = new ArrayList<>(  );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_test );
        mRecyclerView = findViewById( R.id.RecyclerView );
        InitAdapter();
        InitData();
    }
    private void InitAdapter(){
        mRecyclerView.setLayoutManager( new LinearLayoutManager( TestActivity.this ) );
        TestAdapter adapter = new TestAdapter( strings );
        mRecyclerView.setAdapter( adapter );
        WeSwipe.attach( mRecyclerView );
    }
    private void InitData(){
        for (int i = 0; i <3 ; i++) {
            strings.add( "示例"+i );
        }
    }
}
