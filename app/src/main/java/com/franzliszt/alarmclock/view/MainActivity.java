package com.franzliszt.alarmclock.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.franzliszt.alarmclock.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
    }
}
