package com.franzliszt.alarmclock.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.franzliszt.alarmclock.R;

import java.util.ArrayList;
import java.util.List;

import cn.we.swipe.helper.WeSwipeHelper;
import cn.we.swipe.helper.WeSwipeProxyAdapter;

public class TestAdapter extends WeSwipeProxyAdapter<TestAdapter.ViewHolder> {
    List<String> data = new ArrayList<>(  );
    public TestAdapter(List<String> data ){
        this.data = data;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.booking_recyclerview_item,parent,false );
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Test1.setText( data.get( 0 ).toString() );
        holder.Test2.setText( data.get( 1 ).toString() );
        holder.Test3.setText( data.get( 2 ).toString() );
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements WeSwipeHelper.SwipeLayoutTypeCallBack
    {
        private TextView Test1,Test2,Test3,Delete;
        private LinearLayout Layout;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            Test1 = itemView.findViewById( R.id.Test1 );
            Test2 = itemView.findViewById( R.id.Test2 );
            Test3 = itemView.findViewById( R.id.Test3 );
            Delete = itemView.findViewById( R.id.Delete );
            Layout = itemView.findViewById( R.id.Layout );
        }

        @Override
        public float getSwipeWidth() {
            return Delete.getWidth();
        }

        @Override
        public View needSwipeLayout() {
            return Layout;
        }

        @Override
        public View onScreenView() {
            return Layout;
        }
    }
}
